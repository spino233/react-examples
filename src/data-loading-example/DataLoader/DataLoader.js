import React, { Component } from 'react';

export class DataLoader extends Component {
  // class field syntax to avoid boilerplate constructor code
  state = { data: [] }

  componentDidMount() {
    fetch('http://localhost:3001/users')
      .then(res => res.json())
      .then(data => this.setState({ data }));
  }

  render() {
    const { data } = this.state;

    // example of render props
    const { render } = this.props;
    return render(data);
  }
}
