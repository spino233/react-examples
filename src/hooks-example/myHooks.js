import { useEffect, useState } from 'react';

export function useData(url) {
  const [data, setData] = useState([]);

  const getData = async (url) => {
    const res = await fetch(url);
    const data = await res.json();
    setData(data);
  }

  useEffect(() => {
    getData(url);
  },[url]);

  return data;
}
