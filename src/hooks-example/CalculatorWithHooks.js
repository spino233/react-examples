import TemperatureInput from "../calculator-react-tutorial/TemperatureInput";
import BoilingVerdict from "../calculator-react-tutorial/BoilingVerdict";
import { useState } from "react";

export default function CalculatorWithHooks(props) {

  const [temperature, setTemperature] = useState('');
  const [scale, setScale] = useState('');

  const handleCelsiusStateChange = (temperature) => {
    setTemperature(temperature);
    setScale('c');
  }

  const handleFahrenheitStateChange = (temperature) => {
    setTemperature(temperature);
    setScale('f');
  }

  const toCelsius = (fahrenheit) => {
    return (fahrenheit - 32) * 5 / 9;
  }

  const toFahrenheit = (celsius) => {
    return (celsius * 9 / 5) + 32;
  }

  const tryConvert = (temperature, converter) => {
    const input = parseFloat(temperature);
    if (Number.isNaN(input)) {
      return '';
    }
    const output = converter(input);
    const roundedInput = Math.round(output * 1000) / 1000
    return roundedInput.toString();
  }

  const celsius = scale === 'c' ? temperature : tryConvert(temperature, toCelsius);
  const fahrenheit = scale === 'f' ? temperature : tryConvert(temperature, toFahrenheit);

  return (
    <div>
      <TemperatureInput temperature={ celsius }
                        onTemperatureChange={ handleCelsiusStateChange }
                        scale={ 'c' }
      />
      <TemperatureInput scale={ 'f' }
                        temperature={ fahrenheit }
                        onTemperatureChange={ handleFahrenheitStateChange }
      />
      <BoilingVerdict temperature={ parseFloat(celsius) }/>
    </div>
  );

}
