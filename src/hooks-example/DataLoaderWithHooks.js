import { useEffect, useState } from "react";

export const DataLoaderWithHooks = (props) => {
  const [data, setData] = useState([]);

  const getData = async () => {
    const res = await fetch('http://localhost:3001/users');
    const data = await res.json();
    setData(data);
  }

  useEffect(() => {
    getData();
  })

  return props.render(data);
}
