import { useState } from "react";

export default function ExampleHook(){
  const [name, setName] = useState('');

  const handleChangeInput = (e) => {
    const {value} = e.target;
    return setName(value);
  }
  return (
    <div>
      <input onChange={handleChangeInput}/>
      <p>{name}</p>
    </div>
  );

}
