import './App.css';
import FormExample from "./ui-bootstrap-example/FormExample/FormExample";
import { Container, Row } from "react-bootstrap";
import React from "react";
import CustomJumbotron from "./ui-bootstrap-example/CustomJumbotron";
import CustomBlackTable from "./ui-bootstrap-example/CustomBlackTable";
import { DataLoaderWithHooks } from "./hooks-example/DataLoaderWithHooks";
import { ExampleRouter } from "./router-example/ExampleRouter/ExampleRouter";

function App() {
  const formExample = (
    <Container fluid={ true }>
      <Row md={ 1 }>
        <CustomJumbotron
          title={ 'Hey, Gian Marco!' }
          body={ 'Sei veramente un bravo ragazzo.' }
          buttonText={ 'A presto..' }
        />
      </Row>
      <FormExample/>
    </Container>
  );

  const renderTableWithData = (data) => {
    return (
      <CustomBlackTable data={ data }/>
    );
  }

  const dataTable = (<DataLoaderWithHooks render={ renderTableWithData }/>);
  const router = (<ExampleRouter/>)

  console.log(formExample);
  console.log(dataTable);

  return router;
}

export default App;
