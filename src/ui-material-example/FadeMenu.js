import { useState } from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Fade from '@material-ui/core/Fade';
import { Link } from "react-router-dom";
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { IconButton } from "@material-ui/core";

export const FadeMenu = (props) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const { routes } = props;
  return (
    <div>
      <IconButton
        aria-label="more"
        aria-controls="long-menu"
        aria-haspopup="true"
        onClick={ handleClick }
      >
        <MoreVertIcon/>
      </IconButton>
      <Menu
        id="fade-menu"
        anchorEl={ anchorEl }
        keepMounted
        open={ open }
        onClose={ handleClose }
        TransitionComponent={ Fade }
      >
        { routes.map(r => (
          <MenuItem>
            <Link to={ '/' + r.toLowerCase() }>{ r }</Link>
          </MenuItem>)) }
      </Menu>
    </div>
  );
}
