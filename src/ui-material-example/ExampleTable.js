import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';

const useStyles = makeStyles({
  table: {
    minWidth: 350,
  },
});

export const ExampleTable = (props) => {
  const classes = useStyles();
  const { rows, fields } = props;
  return (
    <TableContainer component={ Paper }>
      <Table className={ classes.table } aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align={ 'center' }>ID</TableCell>
            {
              fields.map(f => (
                <TableCell align="center">{ f.charAt(0).toUpperCase() + f.slice(1) }</TableCell>)
              )
            }
          </TableRow>
        </TableHead>
        <TableBody>
          { rows.map((row) => (
            <TableRow key={ row.id }>
              <TableCell align={ 'center' } component="th" scope="row">
                { row.id }
              </TableCell>
              { fields.map(f => (<TableCell align="center">{ row[f] }</TableCell>)) }
            </TableRow>
          )) }
        </TableBody>
      </Table>
    </TableContainer>
  );
}
