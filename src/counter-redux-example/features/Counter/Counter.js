import React, { useState } from "react";
import { counterSelector, decrement, decrementByAmount, increment, incrementByAmount } from "./counterSlice";
import { useDispatch, useSelector } from "react-redux";
import { Badge, Button, Col, Container, Row } from "react-bootstrap";
import './Counter.css';
import CustomJumbotron from "../../../ui-bootstrap-example/CustomJumbotron";

export function Counter() {
  const count = useSelector(counterSelector);
  const dispatch = useDispatch();
  const [incrementAmount, setIncrementAmount] = useState('2');
  const [decrementAmount, setDecrementAmount] = useState('1');

  return (
    <Container fluid style={{backgroundColor: '#FFA07A'}}>
      <CustomJumbotron title={ 'Ciao, Motherfuckers!' }
                       body={ 'Questa è la mia prima implementazione in Redux di uno schifosissimo counter.' }
                       buttonText={ 'Clicca qui per andartene a fanculo =)' }
      />
      <Row className={ 'separateRow justify-content-md-center' }>
        <h1>
          Guarda che counter <Badge variant="secondary">New</Badge>
        </h1>
      </Row>
      <Row className={ 'separateRow justify-content-md-center' }>
        <Col md={ 'auto' }>
          <Button style={{fontSize: '16px'}} onClick={ () => dispatch(increment()) } variant={ 'primary' }>+</Button>
          <span style={ { fontSize: '40px', padding: '0 10px' } }>{ count }</span>
          <Button style={{fontSize: '16px'}} onClick={ () => dispatch(decrement()) } variant={ 'danger' }>-</Button>
        </Col>
      </Row>
      <Row>
        <h3>-------- +/- Valore personalizzato --------</h3>
      </Row>
      <Row className={ 'separateRow justify-content-md-center' }>
        <Col>
          <input type={ 'text' }
                 value={ incrementAmount }
                 onChange={ (e) => setIncrementAmount(e.target.value) }
                 style={ { fontSize: '25px', textAlign: 'center', marginRight: '5px' } }/>
          <Button variant={ 'primary' }
                  onClick={ () => dispatch(incrementByAmount(Number(incrementAmount) || 0)) }
                  style={ { fontSize: '25px' } }>
            Aggiungilo
          </Button>
        </Col>
      </Row>
      <Row className={ 'separateRow justify-content-md-center' }>
        <Col>
          <div>
            <input type={ 'text' }
                   value={ decrementAmount }
                   onChange={ (e) => setDecrementAmount(e.target.value) }
                   style={ {
                     fontSize: '25px',
                     textAlign: 'center',
                     marginRight: '5px'
                   } }/>
            <Button style={ { fontSize: '25px' } }
                    variant={ 'danger' }
                    onClick={ () => dispatch(decrementByAmount(Number(decrementAmount) || 0)) }>
              Sottrailooo
            </Button>
          </div>

        </Col>
      </Row>
    </Container>
  )
}
