import {createSlice} from "@reduxjs/toolkit";

export const slice = createSlice({
    name: 'counter',
    initialState: {
      value: 10
    },
    reducers: {
      increment : state => {
        state.value += 1
      },
      decrement: state => {
        state.value -=1
      },
      incrementByAmount: (state, action) => {
        state.value = state.value + action.payload;
      },
      decrementByAmount: (state, action) => {
        state.value = state.value - action.payload;
      },
    },
});

export const {increment, decrement, incrementByAmount, decrementByAmount} = slice.actions;

export const counterSelector = state => state.counter.value;

export default slice.reducer;
