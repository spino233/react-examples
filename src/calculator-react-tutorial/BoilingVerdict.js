export default function BoilingVerdict(props) {
  if (props.temperature >= 100) {
    return <p>Water will boil.</p>;
  }
  return <p>Water will not boil.</p>
}
