const scaleNames = {
  c: 'Celsius',
  f: 'Fahrenheit',
}

export default function TemperatureInput(props) {
  const { temperature, scale, onTemperatureChange } = props;
  const handleChange = (e) => {
    const { value } = e.target;
    onTemperatureChange(value);
  };

  return (
    <fieldset>
      <legend>Enter Temperature in { scaleNames[scale] }</legend>
      <input name='temperature' onChange={ handleChange } value={ temperature }/>
    </fieldset>
  );
}
