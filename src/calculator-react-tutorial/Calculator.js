import React from "react";
import TemperatureInput from "./TemperatureInput";
import BoilingVerdict from "./BoilingVerdict";


export default class Calculator extends React.Component {

  constructor(props) {
    super(props);
    this.handleCelsiusStateChange = this.handleCelsiusStateChange.bind(this);
    this.handleFahrenheitStateChange = this.handleFahrenheitStateChange.bind(this);
    this.state = {
      temperature: '',
      scale: '',
    }
  }

  handleCelsiusStateChange(temperature) {
    this.setState({
      temperature,
      scale: 'c',
    });
  }

  handleFahrenheitStateChange(temperature) {
    this.setState({
      temperature,
      scale: 'f',
    })
  }


  toCelsius(fahrenheit) {
    return (fahrenheit - 32) * 5 / 9;
  }

  toFahrenheit(celsius) {
    return (celsius * 9 / 5) + 32;
  }

  tryConvert(temperature, converter) {
    const input = parseFloat(temperature);
    if (Number.isNaN(input)) {
      return '';
    }
    const output = converter(input);
    const roundedInput = Math.round(output * 1000) / 1000
    return roundedInput.toString();
  }

  render() {
    const { temperature, scale } = this.state;
    const celsius = scale === 'c' ? temperature : this.tryConvert(temperature, this.toCelsius);
    const fahrenheit = scale === 'f' ? temperature : this.tryConvert(temperature, this.toFahrenheit);
    return (
      <div>
        <TemperatureInput temperature={ celsius }
                          onTemperatureChange={ this.handleCelsiusStateChange }
                          scale={ 'c' }
        />
        <TemperatureInput scale={ 'f' }
                          temperature={ fahrenheit }
                          onTemperatureChange={ this.handleFahrenheitStateChange }
        />
        <BoilingVerdict temperature={ parseFloat(celsius) }/>
      </div>
    );
  }

}

