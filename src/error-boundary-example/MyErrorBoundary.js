import React from "react";

export default class MyErrorBoundary extends React.Component {

  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    const hasError = MyErrorBoundary.getDerivedStateFromError(error);
    this.setState({
      hasError
    })
  }

  render() {
    const { hasError } = this.state;
    if (hasError) {
      return <p>Something went wrong</p>
    }
    return this.props.children;
  }
}
