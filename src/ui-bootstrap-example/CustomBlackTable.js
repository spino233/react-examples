import React from "react";
import { Table } from "react-bootstrap";

export default function CustomBlackTable(props) {
  const { data } = props;
  return (
    <Table striped bordered hover variant="dark">
      <thead>
      <tr>
        <th>#</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Age</th>
      </tr>
      </thead>
      <tbody>
      { data.map(user => {
        return (
          <tr key={ user.id }>
            <td>{ user.id }</td>
            <td>{ user.name }</td>
            <td>{ user.surname }</td>
            <td>{ user.age }</td>
          </tr>
        )
      }) }
      </tbody>
    </Table>
  )
}
