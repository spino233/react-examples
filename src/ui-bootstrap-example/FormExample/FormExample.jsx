import { Button, Form } from 'react-bootstrap';
import { useState } from "react";

export default function FormExample() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [check, setCheck] = useState(false);

  const handleInputTextChange = (e) => {
    console.log(e);
    const { value, name } = e.target;
    if (name === 'email') {
      setEmail(value);
    } else {
      setPassword(value);
    }
  }

  const handleCheckChange = (e) => {
    console.log(e);
    const { checked } = e.target;
    setCheck(checked);
  }

  return (
    <Form>
      <Form.Group controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control name="email" onChange={ handleInputTextChange } type="email"
                      placeholder="Inserisci la tua mail capo!"/>
        <Form.Text className="text-muted">
          Tranquillo, rimane solo tra me e te..
        </Form.Text>
      </Form.Group>

      <Form.Group controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control name="password" onChange={ handleInputTextChange } type="password"
                      placeholder="Scrivi la password, ma non farla vedere.."/>
      </Form.Group>
      <Form.Group controlId="formBasicCheckbox">
        <Form.Check type="checkbox" onChange={ handleCheckChange } label="Ti piace?"/>
      </Form.Group>
      <Button variant="primary" type="submit">
        Submit
      </Button>
      <span>{email}|{password}|{check.toString()}</span>
    </Form>
  )
}
