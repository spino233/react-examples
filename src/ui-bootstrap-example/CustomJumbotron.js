import React from "react";
import { Button, Jumbotron } from "react-bootstrap";

export default function CustomJumbotron(props) {
  const { title, body, buttonText } = props;
  return (
    <Jumbotron style={{backgroundColor: '#FF0000'}}>
      <h1>{ title }</h1>
      <p>
        { body }
      </p>
      <p>
        <Button variant="primary">{ buttonText }</Button>
      </p>
    </Jumbotron>
  )
}
