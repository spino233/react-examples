import React from "react";
import { Card } from "react-bootstrap";
import "./CustomCard.css";

export default function CustomCard(props){
  const {title, subTitle, body, linkOne, linkTwo} = props;
  return (
    <Card className={'myCard'}>
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">{subTitle}</Card.Subtitle>
        <Card.Text>
          { body }
        </Card.Text>
        <Card.Link href="#">{ linkOne }</Card.Link>
        <Card.Link href="#">{ linkTwo }</Card.Link>
      </Card.Body>
    </Card>
  )
}
