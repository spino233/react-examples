import { Link } from "react-router-dom";
import './NavBar.css';

export const NavBar = (props) => {
  return (
    <div className={ 'flexbox-container' }>
      <nav>
        <ul>
          { props.routes.map(r => {
            return (
              <li key={ r } className={ 'flexbox-item' }>
                <Link style={{display: 'inline-block'}} to={ r.path }>{ r.name }</Link>
              </li>
            );
          }) }
        </ul>
      </nav>
    </div>
  );
}
