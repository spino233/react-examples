import { BrowserRouter as Router, Route } from "react-router-dom";
import Switch from "react-bootstrap/Switch";
import { ExampleTable } from "../../ui-material-example/ExampleTable";
import { useData } from "../../hooks-example/myHooks";
import { NavBar } from "../NavBar/NavBar";

const routes = [
  { name: 'Home', path: '/home' },
  { name: 'About Us', path: '/aboutus' },
  { name: 'Contacts', path: '/contacts' },
  { name: 'Users', path: '/users' },
];


export const ExampleRouter = () => {
  return (
    <Router>
      <NavBar routes={ routes }/>
      <Switch style={{paddingRight: '2.25rem'}}>
        <Route path={ '/contacts' }>
          <Contacts/>
        </Route>
        <Route path={ '/aboutus' }>
          <AboutUs/>
        </Route>
        <Route path={ '/users' }>
          <Users/>
        </Route>
        <Route path={ '/home' }>
          <Home/>
        </Route>
      </Switch>
    </Router>
  );
}

const Home = () => {
  return (
    <div style={ { display: 'flex', justifyContent: 'center' } }>
      <h1>HELLO HOME</h1>
    </div>
  )
}

const Contacts = () => {
  return (
    <div style={ { display: 'flex', justifyContent: 'center' } }>
      <h1>Contacts</h1>
    </div>
  )
}
const AboutUs = () => {
  return (
    <div style={ { display: 'flex', justifyContent: 'center' } }>
      <h1>About Us</h1>
    </div>
  )
}
const Users = () => {
  const fields = [
    'name',
    'surname',
    'age'
  ];

  const data = useData('http://localhost:3001/users')

  return (
    <div style={ { display: 'flex', justifyContent: 'center' } }>
      <ExampleTable rows={ data } fields={ fields }/>
    </div>
  )
}



